import matplotlib.pyplot as plt
	
def main(args):
	
	fin = open('in')
	indata = []
	for i in fin:
		indata.append(int(i, 0))
	print(indata.pop(len(indata)-1))
	
	fout = open('out')
	outdata = []
	for i in fout:
		outdata.append(int(i, 0))
	print(outdata)
	
	win = str(5)
	if len(args) > 1:
		win = args[1]
	
	plt.figure()
	plt.title('Фильтрация с окном - ' + win)
	plt.ylim(0,255)
	plt.plot(range(len(indata)), indata, 'r', range(len(outdata)), outdata, 'b')
	plt.grid(True) 
	plt.show()
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
