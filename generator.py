import random

def main(args):
	for i in range(100):
		gen = random.randint(128 - 32, 128 + 32)
		if (random.randint(0,7) == 0):
			gen += random.randint(-64, 64)
		print(hex(gen))	
	print(0)

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
