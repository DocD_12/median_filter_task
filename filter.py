import sys

def buf_init(buf, win, first_el):
	for i in range(win):
		buf.append(first_el)
	return buf
	
def buf_add(buf, head, el):
	buf[head] = el
	head += 1
	if head >= len(buf):
		head = 0
	return buf, head	
	
def median(buf):
	buf.sort()
	return buf[len(buf) // 2]	
	
def main(args):
	
	win = 5
	if len(args) > 1:
		win = int(args[1])
	
	buf = []
	header = 0
	init_flag = False
	
	while 1:
		data = int(input(), 0)
		
		if not init_flag:
			buf_init(buf, win, data)
			init_flag = True
		else:
			buf, header = buf_add(buf, header, data)
			
		if data == 0: break
		
		data = median(buf)
		
		print(hex(data))
		
if __name__ == '__main__':
    sys.exit(main(sys.argv))
